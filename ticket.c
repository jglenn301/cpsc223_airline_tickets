#include "ticket.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct segment
{
  char orig[4];
  char dest[4];
} segment;

struct ticket
{
  segment *segments;
  int count;
  int capacity;
};

const int INITIAL_SEGMENT_CAPACITY = 4;

/**
 * Resized the segment array by doubling its size.  There is no effect if
 * there is a memory allocation error.
 *
 * @param t a pointer to a valid ticket
 */
void ticket_embiggen(ticket *t);

ticket *ticket_create()
{
  ticket * t = malloc(sizeof(ticket));
  if (t != NULL)
    {
      t->count = 0;
      t->segments = malloc(sizeof(segment) * INITIAL_SEGMENT_CAPACITY);
      t->capacity = t->segments != NULL ? INITIAL_SEGMENT_CAPACITY : 0;
    }
  return t;
}

void ticket_destroy(ticket *t)
{
  if (t != NULL)
    {
      free(t->segments);
      free(t);
    }
}

void ticket_add_segment(ticket *t, const char *origin, const char *destination)
{
  // validate arguments
  if (t == NULL || strlen(origin) != 3 || strlen(destination) != 3)
    {
      return;
    }

  // resize if necessary
  if (t->count == t->capacity)
    {
      ticket_embiggen(t);
    }

  // add if there is room
  if (t->count < t->capacity)
    {
      strcpy(t->segments[t->count].orig, origin);
      strcpy(t->segments[t->count].dest, destination);
      t->count++;
    }
}

void ticket_embiggen(ticket *t)
{
  segment *bigger = realloc(t->segments, sizeof(segment) * t->capacity * 2);
  if (bigger != NULL)
    {
      t->segments = bigger;
      t->capacity *= 2;
    }
}

int ticket_count_flights(const ticket *t, const char *airport)
{
  if (t == NULL || airport == NULL)
    {
      return 0;
    }

  // compare origin and destination of each segment to argument
  int count = 0;
  for (int i = 0; i < t->count; i++)
    {
      if (strcmp(t->segments[i].orig, airport) == 0 || strcmp(t->segments[i].dest, airport) == 0)
	{
	  count++;
	}
    }
  return count;
}

bool ticket_has_ground_segment(const ticket *t)
{
  if (t == NULL)
    {
      return false;
    }

  // search for a segment where the destination doesn't match the next origin
  int i = 0;
  while (i < t->count - 1 && strcmp(t->segments[i].dest, t->segments[i + 1].orig) == 0)
    {
      i++;
    }

  return i < t->count - 1;
}
