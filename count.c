#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ticket.h"

#define INITIAL_CAPACITY 10

/**
 * Reads and returns one line of input from the given file.  The
 * newline character is not added to the string.  If the file is
 * already at EOF then the returned value is NULL.  Otherwise, it is
 * the caller's responsibility to free the returned string.
 *
 * @param in an input file
 * @return a pointer to a dynamically allocate string, or NULL
 */
char *read_line(FILE *in);

/**
 * Destroys all the tickets in the given array and frees the array.
 *
 * @param tickets a pointer to a dynamically allocated array of tickets
 * @param n the number of valid tickets in that array
 */
void destroy_all_tickets(ticket **tickets, int n);

int main(int argc, char **argv)
{
  ticket **tickets = malloc(sizeof(ticket *) * INITIAL_CAPACITY);
  if (tickets == NULL)
    {
      fprintf(stderr, "%s: couldn't allocate tickets array\n", argv[0]);
      return 1;
    }
  tickets[0] = NULL;
  
  int curr_ticket = 0;
  int ticket_count = 0;
  int ticket_capacity = INITIAL_CAPACITY;

  char *line;
  while ((line = read_line(stdin)) != NULL)
    {
      if (strlen(line) == 0)
	{
	  // blank line between tickets; move to next ticket and
	  // initialize it to NULL (will create next ticket when we
	  // see its first segment)
	  curr_ticket++;
	  if (curr_ticket < ticket_capacity)
	    {
	      tickets[curr_ticket] = NULL;
	    }
	}
      else
	{
	  // non-blank line: a segment
	  if (strlen(line) % 3 != 0)
	    {
	      // length is not a multiple of three, so input is not
	      // a sequence of 3-character airport codes
	      fprintf(stderr, "%s: invalid input %s\n", argv[0], line);
	      destroy_all_tickets(tickets, ticket_count);
	      return 1;
	    }
	  
	  if (curr_ticket == ticket_capacity || tickets[curr_ticket] == NULL)
	    {
	      // 1st segment in new ticket -- create the ticket, resizing
	      // array if needed to hold it
	      if (curr_ticket == ticket_capacity)
		{
		  ticket **bigger = malloc(sizeof(ticket *) * ticket_capacity * 2);
		  if (bigger != NULL)
		    {
		      memcpy(bigger, tickets, sizeof(ticket *) * ticket_capacity);
		      free(tickets);
		      tickets = bigger;
		      ticket_capacity *= 2;
		    }
		  else
		    {
		      fprintf(stderr, "%s: memory allocation error\n", argv[0]);
		      destroy_all_tickets(tickets, ticket_count);
		      return 1;
		    }
		}
	      
	      tickets[curr_ticket] = ticket_create();
	      if (tickets[curr_ticket] == NULL)
		{
		  fprintf(stderr, "%s: memory allocation error\n", argv[0]);
		  destroy_all_tickets(tickets, ticket_count);
		  return 1;
		}
	      ticket_count++;
	    }

	  // parse the line of input and add segments to ticket
	  int segment_count = strlen(line) / 3 - 1;
	  for (int s = 0; s < segment_count; s++)
	    {
	      // copy 3-character substrings for origin and destination
	      char orig[4];
	      char dest[4];
	      strncpy(orig, &line[s * 3], 3);
	      strncpy(dest, &line[s * 3 + 3], 3);
	      orig[3] = dest[3] = '\0';

	      // add the segment to the ticket
	      ticket_add_segment(tickets[curr_ticket], orig, dest);
	    }
	}
      
      free(line);
    }

  // for each command-line argument, count number of flights using it
  for (int a = 1; a < argc; a++)
    {
      if (strlen(argv[a]) != 3)
	{
	  fprintf(stderr, "%s: invalid code %s\n", argv[0], argv[a]);	
	  destroy_all_tickets(tickets, ticket_count);
	}

      int count = 0;
      for (int i = 0; i < ticket_count; i++)
	{
	  count += ticket_count_flights(tickets[i], argv[a]);
	}
      printf("%s: %d\n", argv[a], count);
    }

  destroy_all_tickets(tickets, ticket_count);

  return 0;
}

void destroy_all_tickets(ticket **tickets, int n)
{
  for (int i = 0; i < n; i++)
    {
      ticket_destroy(tickets[i]);
    }
  free(tickets);
}

char *read_line(FILE *in)
{
  // allocate initial space and null-terminate it
  char *line = malloc(sizeof(char) * (INITIAL_CAPACITY + 1));
  int len = 0;
  int capacity = line != NULL ? INITIAL_CAPACITY : 0;
  if (line != NULL)
    {
      line[0] = '\0';
    }

  // loop until end of line or end of file
  int ch;
  while ((ch = fgetc(in)) != EOF && ch != '\n')
    {
      // if we have a place to store the character, store it
      if (line != NULL)
	{
	  // check whether we need to resize
	  if (len == capacity)
	    {
	      char *bigger = malloc(sizeof(char) * (capacity * 2 + 1));
	      if (bigger != NULL)
		{
		  strcpy(bigger, line);
		  free(line);
		  line = bigger;
		  capacity *= 2;
		}
	      else
		{
		  // resize failed
		  free(line);
		  line = NULL;
		}
	    }

	  // add new character and move null character
	  if (line != NULL)
	    {
	      line[len++] = ch;
	      line[len] = '\0';
	    }
	}
    }

  // strip trailing carriage returns
  char *end = line + len - 1;
  while (end >= line && *end == '\r')
    {
      *(end--) = '\0';
    }

  if (ch == EOF && len == 0)
    {
      // nothing to read; return NULL
      free(line);
      line = NULL;
    }

  return line;
}
