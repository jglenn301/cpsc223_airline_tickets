CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -g3

Count: count.o ticket.o
	${CC} ${CFLAGS} -o $@ $^

count.o: ticket.h
ticket.o: ticket.h

clean:
	rm -r *.o Count
