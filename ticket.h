#ifndef __TICKET_H__
#define __TICKET_H__

#include <stdbool.h>

struct ticket;
typedef struct ticket ticket;

/**
 * Creates an empty ticket.  Returns NULL if there is a memory
 * allocation error.  Othwewise, it is the caller's responsibility to
 * eventually destroy the returned ticket.
 *
 * @return a pointer to a new empty ticket
 */
ticket *ticket_create();

/**
 * Destroys a ticket.
 *
 * @param a pointer to a valid ticket
 */
void ticket_destroy(ticket *t);

/**
 * Adds a flight segment to the given ticket.  There is no effect
 * on the ticket if there is a memory allocation error.
 *
 * @param t a pointer to a valid ticket
 * @param origin a pointer to a three-character string
 * @param destination a pointer to a three-character string
 */
void ticket_add_segment(ticket *t, const char *origin, const char *destination);

/**
 * Counts the number of flights on this ticket that use the given
 * airport.  A connection point counts as two flights.
 *
 * @param t a pointer to a valid ticket
 * @param airport a pointer to a three-character string
 * @return the number of flights that use that airport
 */
int ticket_count_flights(const ticket *t, const char *airport);

/**
 * Determines if this ticket has a ground segment.  A ticket has
 * a ground segment if one flight leaves from a different airport
 * than the previous flight arrived at.
 *
 * @param t a pointer to a valid ticket
 * @return true if and only if this ticket has a ground segment
 */
bool ticket_has_ground_segment(const ticket *t);

#endif

